* @ValidationCode : Mjo4OTQ1MDI3NDk6Q3AxMjUyOjE1OTU4NDQxNzMxNzg6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Jul 2020 13:02:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SG.Demo
*
* Implementation of SG.Demo.ValidationRtn
*
*
SUBROUTINE V.VALIDATION.RTN    
    $USING EB.SystemTables
    $USING AC.AccountOpening
    $USING ST.Customer
    $USING ST.Config
    $USING FT.Contract
    $USING EB.ErrorProcessing
    
    GOSUB Init
    RETURN
    
Init:
    fieldPos = EB.SystemTables.getAf()
    creditAcctNo = EB.SystemTables.getComi()
    paramRec = SG.Demo.getParamRec()
    
    GOSUB GetDebitAcct
RETURN

GetDebitAcct:
    accountRec = AC.AccountOpening.Account.Read(creditAcctNo, err)
    creditCcy = accountRec<AC.AccountOpening.Account.Currency>
    currencies = paramRec<SG.Demo.DemoParam.CurrencyCode>
        
    CHANGE @VM TO @FM IN currencies
    LOCATE creditCcy IN currencies SETTING pos THEN
        suspenseAcct = paramRec<SG.Demo.DemoParam.SuspenseAcct, pos>
    END ELSE
        EB.SystemTables.setEtext("Currency not supported")
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf(FT.Contract.FundsTransfer.CreditCurrency)
        
        RETURN
    END
    
    GOSUB GetCreditRef
RETURN

GetCreditRef:
    creditCustomer = accountRec<AC.AccountOpening.Account.Customer>
    customerRec = ST.Customer.Customer.Read(creditCustomer, err)
    nationality = customerRec<ST.Customer.Customer.EbCusNationality>
    countries = paramRec<EB.Demo.DemoParam.CountryCode>
    CHANGE @VM TO @FM IN countries
    LOCATE nationality IN countries SETTING pos ELSE
        EB.SystemTables.setEtext("Country not supported")
        EB.ErrorProcessing.StoreEndError()
        EB.SystemTables.setAf(FT.Contract.FundsTransfer.CreditCurrency)
        RETURN
    END
    
    countryRec = ST.Config.Country.CacheRead(nationality, err)
    country = countryRec<ST.Config.Country.EbCouGeographicalBlock>
    creditRef = SG.Demo.Concat("From ", TRIM(country))
    
    GOSUB UpdateValues
RETURN

UpdateValues:
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.CreditCurrency, creditCcy)
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.CreditTheirRef, creditRef)
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.DebitAcctNo, suspenseAcct)
RETURN

END
