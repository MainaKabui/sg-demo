* @ValidationCode : MjotNDM5NTUxNzAwOkNwMTI1MjoxNTk1ODQzNDQ4NzU5Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Jul 2020 12:50:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SG.Demo

SUBROUTINE V.CHECK.RTN
    $USING EB.SystemTables
    $USING FT.Contract
    $USING EB.LocalReferences

    transType = "AC"
    paramRec = SG.Demo.GetParam()
    SG.Demo.setParamRec(Value)
                      
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.OrderingBank, SG.Demo.BankName)
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.TransactionType, transType)
        
    appName = "FUNDS.TRANSFER"
    fieldName = "CHANNEL.REF"
    
    EB.LocalReferences.GetLocRef(appName, fieldName, pos)
    channelRef<1,pos> = "REF10001"
    EB.SystemTables.setRNew(FT.Contract.FundsTransfer.LocalRef, channelRef)
END
