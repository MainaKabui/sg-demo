* @ValidationCode : MjotMzM2OTI5NjcxOkNwMTI1MjoxNTk1ODQ1NTg5MTU3Ok1haW5hOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjE5X1NQMjQuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Jul 2020 13:26:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SG.Demo

SUBROUTINE V.AUTH.RTN
    $USING EB.SystemTables
    $USING EB.DataAccess
    $USING FT.Contract
    $USING ST.Customer
    $USING EB.Foundation
    $USING EB.Interface
    
Init:
    appName = "CUSTOMER"
    ofsFunc = "I"
    process = "PROCESS"
    versionName = "CUSTOMER,DEMO"
    gtsMode = ""
    noOfAuth = 0
    ofsId = 'GENERIC.OFS.PROCESS'
    
    id = EB.SystemTables.getIdNew()
    creditCust = EB.SystemTables.getRNew(FT.Contract.FundsTransfer.CreditCustomer)
    
    GOSUB PostOfs
RETURN

PostOfs:
	aa = "Hello"
	bb = "World"
    custRec = ST.Customer.Customer.CacheRead(creditCust, err)
    custRec<ST.Customer.Customer.EbCusAccountOfficer> = 1
    EB.Foundation.OfsBuildRecord(appName, ofsFunc, process, versionName, gtsMode, noOfAuth, creditCust, custRec, ofsReq)
    
    ofsSourceRec = EB.Interface.OfsSource.Read(ofsId, error)
    EB.Interface.setOfsSourceId(ofsId);
    EB.Interface.setOfsSourceRec(ofsSourceRec);
    EB.Interface.OfsProcessManager(ofsReq, ofsRes)
    
    GOSUB WriteToFile
RETURN
    
WriteToFile:
    fnFile = "LOG.BP"; fFile = ""
    EB.DataAccess.Opf(fnFile, fFile)
    
    rec<-1>="Hello"
    rec<-1>="World"
    EB.DataAccess.FWrite(fFile, id, rec)
RETURN

END
