* @ValidationCode : Mjo3OTAyODUxNTg6Q3AxMjUyOjE1OTU4NDIxOTE2MTQ6TWFpbmE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMTlfU1AyNC4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Jul 2020 12:29:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Maina
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R19_SP24.0
$PACKAGE SG.Demo

FUNCTION GET.DEMO.PARAM
    rec = SG.Demo.DemoParam.CacheRead("SYSTEM", err)
   
RETURN(rec)

END
